
const FIRST_NAME = "Cristina";
const LAST_NAME = "Suciu";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
function numberParser(value) {

if(typeof(value)=="number"&&(value>Number.MAX_SAFE_INTEGER||value<Number.MIN_SAFE_INTEGER))
{
    return NaN
}

    return parseInt(value)
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

