
const FIRST_NAME = "Cristina";
const LAST_NAME = "Suciu";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
function initCaching() {
   var cache={};
   var obiect={};
   cache.pageAccessCounter=function(valoare)
   {
       if(valoare==null && obiect.hasOwnProperty('home'))
       obiect['home']=obiect['home']+1;
       else if(valoare==null && obiect.hasOwnProperty('home')==false)
       obiect['home']=1;
else{
      var nume=String(valoare).toLocaleLowerCase()
       if(obiect.hasOwnProperty(nume))
       obiect[nume]=obiect[nume]+1;
       else 
       obiect[nume]=1;
}

   };
   cache.getCache=function()
   {
return obiect;
   };
   return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

